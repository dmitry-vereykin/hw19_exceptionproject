/**
 * Created by Dmitry on 7/8/2015.
 */
public class InvalidPayRate extends Exception {
    public InvalidPayRate() {
        super("ERROR: Negative pay rate. ");
    }
}
